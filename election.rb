#!/usr/bin/env ruby
# Election script by Matt Lee, Brett Smith, Matt Anderson, Justin Baugh (and YOU?)
# To the extent possible under law, the person who associated CC0 with
# this election script has waived all copyright and related or neighboring rights.
# You should have received a copy of the CC0 legalcode along with this
# work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
require 'json'
require 'net/http'
require 'date'

electoral_votes = { "AL" => 9, "MT" => 3, "AK" => 3, "NE" => 5, "AZ" => 11,
                    "NV" => 6, "AR" => 6, "NH" => 4, "CA" => 55, "NJ" => 14,
                    "CO" => 9, "NM" => 5, "CT" => 7, "NY" => 29, "DE" => 3,
                    "NC" => 15, "FL" => 29, "ND" => 3, "GA" => 16, "OH" => 18,
                    "HI" => 4, "OK" => 7, "ID" => 4, "OR" => 7, "IL" => 20,
                    "PA" => 20, "IN" => 11, "RI" => 4, "IA" => 6, "SC" => 9,
                    "KS" => 6, "SD" => 3, "KY" => 8, "TN" => 11, "LA" => 8,
                    "TX" => 38, "ME" => 4, "UT" => 6, "MD" => 10, "VT" => 3,
                    "MA" => 11, "VA" => 13, "MI" => 16, "WA" => 12, "MN" => 10,
                    "WV" => 5, "MS" => 6, "WI" => 10, "MO" => 10, "WY" => 3, }

class Float
	def commas
	  self.to_s =~ /([^\.]*)(\..*)?/
	  int, dec = $1.reverse, $2 ? $2 : ""
	  while int.gsub!(/(,|\.|^)(\d{3})(\d)/, '\1\2,\3')
	  end
	  int.reverse + dec
	end
end

class Fixnum
	def commas
	  self.to_s =~ /([^\.]*)(\..*)?/
	  int, dec = $1.reverse, $2 ? $2 : ""
	  while int.gsub!(/(,|\.|^)(\d{3})(\d)/, '\1\2,\3')
	  end
	  int.reverse + dec
	end
end

class Candidate
  attr_reader :name, :color_code
  attr_accessor :votes, :electors

  def initialize(name, color_code="0")
    @name = name
    @color_code = color_code
    @votes = 0
    @electors = 0
  end

  def reset_counts()
    @votes = 0
    @electors =0
  end

  def result_line(total)
    pct = (votes/ Float(total))*100
    sprintf("\033[%sm%9s: %15s\033[0m (%5.2f%%) %3dEV\n", @color_code, @name, votes.commas, pct, electors)
  end
end

candidates = {
  "clinton" => Candidate.new("Clinton", "1;34"),
  "trump" => Candidate.new("Trump", "1;31"),
  "johnson" => Candidate.new("Johnson", "1;33"),
  "stein" => Candidate.new("Stein", "1;32"),
}

if ARGV[1].nil?
  START_SLEEP_TIME = 10
else
  START_SLEEP_TIME = ARGV[1].to_i
end

url = 'https://my.slate.com/votecastr/api/turnout_totals/?filter_name=all&format=json'
uri = URI(url)
sleep_time = 0.01
votes_cast = 0
while true
  sleep sleep_time
  begin
    res = Net::HTTP.get(uri)
    json = JSON.parse(res)
  rescue
    json = {"status" => nil}
  end
  if json['status'] != 200
    sleep_time *= 1.2
    next
  end
  sleep_time = START_SLEEP_TIME
  tally_json = json["data"]
  datawhen = DateTime.parse(json['time'])

  candidates.values.each do |candidate|
    candidate.reset_counts
  end

  tally_json.each do |state, state_data|
    candidates.keys.each { |candidate| candidates[candidate].votes += state_data[candidate]["currentTurnout"] }
    candidates[candidates.keys.max_by { |key| state_data[key]["currentTurnout"] }].electors += electoral_votes[state];
  end

  system("clear")
  printf("Data from Slate at %s GMT\n", datawhen.strftime("%H:%M:%S"))
  votes_cast = candidates.values.map { |c| c.votes }.reduce(:+)
  printf("Total votes cast: #{votes_cast.commas}\n\n")
  candidates.values.each do |candidate|
    print candidate.result_line(votes_cast)
  end
end
